package org.lucky.synonyms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.lucky.synonyms.sqls.ExecuteScripts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@PropertySource("file:/tmp/flyway-prop/application.properties")
public class App implements CommandLineRunner {
	
	@Value("${db.jdbc-url}")
	private String jdbcUrl;
	@Value("${db.username}")
	private String userName;
	@Value("${db.password}")
	private String password;
	
	@Autowired
	private ExecuteScripts executeScripts;
	
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public Connection jdbcConnection() {
		Connection connection=null;
		try {
			 connection = DriverManager.getConnection(jdbcUrl, userName, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Program started....");
		executeScripts.run();
		
	}
	
	
}

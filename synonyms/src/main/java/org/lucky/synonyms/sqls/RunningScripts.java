package org.lucky.synonyms.sqls;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
public class RunningScripts {
   public static void main(String args[]) throws Exception {
      //Registering the Driver
      DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
      //Getting the connection
      String mysqlUrl = "jdbc:oracle:thin:@localhost:1521:orcl1";
      Connection con = DriverManager.getConnection(mysqlUrl, "APCOREUSER", "Oracle123");
      System.out.println("Connection established......");
      //Initialize the script runner
       
      String plsql = "CREATE OR REPLACE SYNONYM APCOREUSER.T_ADDRESS for APCORE.T_ADDRESS";
      
      CallableStatement cstmt = con.prepareCall(plsql);
      cstmt.execute();
   }
}
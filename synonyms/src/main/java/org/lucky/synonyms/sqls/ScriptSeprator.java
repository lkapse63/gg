package org.lucky.synonyms.sqls;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.stereotype.Service;
@Service
public class ScriptSeprator {
	/**
	 * Default statement separator within SQL scripts: {@code ";"}.
	 */
	public static String DEFAULT_STATEMENT_SEPARATOR = "/";

	/**
	 * Fallback statement separator within SQL scripts: {@code "\n"}.
	 * <p>
	 * Used if neither a custom separator nor the
	 * {@link #DEFAULT_STATEMENT_SEPARATOR} is present in a given script.
	 */
	public static final String FALLBACK_STATEMENT_SEPARATOR = "\n";

	/**
	 * End of file (EOF) SQL statement separator: {@code "^^^ END OF SCRIPT ^^^"}.
	 * <p>
	 * This value may be supplied as the {@code separator} to
	 * {@link #executeSqlScript(Connection, EncodedResource, boolean, boolean, String, String, String, String)}
	 * to denote that an SQL script contains a single statement (potentially
	 * spanning multiple lines) with no explicit statement separator. Note that such
	 * a script should not actually contain this value; it is merely a
	 * <em>virtual</em> statement separator.
	 */
	public static String EOF_STATEMENT_SEPARATOR = "^^^ END OF SCRIPT ^^^";

	/**
	 * Default prefix for single-line comments within SQL scripts: {@code "--"}.
	 */
	public static String DEFAULT_COMMENT_PREFIX = "--";

	/**
	 * Default prefixes for single-line comments within SQL scripts: {@code ["--"]}.
	 * 
	 * @since 5.2
	 */
	public static String[] DEFAULT_COMMENT_PREFIXES = { DEFAULT_COMMENT_PREFIX };

	/**
	 * Default start delimiter for block comments within SQL scripts: {@code "/*"}.
	 */
	public static String DEFAULT_BLOCK_COMMENT_START_DELIMITER = "/*";

	/**
	 * Default end delimiter for block comments within SQL scripts:
	 * <code>"*&#47;"</code>.
	 */
	public static String DEFAULT_BLOCK_COMMENT_END_DELIMITER = "*/";
	
	@Value("${script.program.start}")
	private String startProgram;
	
	@Value("${script.grantee}")
	private String grantee;
	
	@Value("${script.grantor}")
	private String grantor;
	
	@Value("${script.placeholder.prefix}")
	private String scriptPlaceholderPrefix;
	
	@Value("${script.placeholder.sufix}")
	private String scriptPlaceholdersufix;
	
	@Value("${script.placeholder.grantee}")
	private String scriptPlaceholderGrantee;
	
	@Value("${script.placeholder.grantor}")
	private String scriptPlaceholderGrantor;

	

	public List<String> blockExtract(File file) {
		List<String> blocksList = new ArrayList<String>();
		String placeHolderGrantee=scriptPlaceholderPrefix+scriptPlaceholderGrantee+scriptPlaceholdersufix;
		String placeHoldergrantor=scriptPlaceholderPrefix+scriptPlaceholderGrantor+scriptPlaceholdersufix;
		Scanner scanner;
		try {
			scanner = new Scanner(file);
			String block = null;

			while (scanner.hasNextLine()) {
				String token = scanner.nextLine()
						.replaceAll(placeHolderGrantee, grantee)
						.replaceAll(placeHoldergrantor, grantor);

				if (token.contains(startProgram)) {
					block = token;
					while (!token.contains(DEFAULT_STATEMENT_SEPARATOR)) {
						token = scanner.nextLine()
								.replaceAll(placeHolderGrantee, grantee)
								.replaceAll(placeHoldergrantor, grantor);
						block = block + FALLBACK_STATEMENT_SEPARATOR + token;
					}
					blocksList.add(block);
				}

				else if (token.contains(DEFAULT_BLOCK_COMMENT_START_DELIMITER)) {
					while ((token = scanner.nextLine()) != null) {
						if (token.contains(DEFAULT_BLOCK_COMMENT_END_DELIMITER))
							break;
					}
				} else if (token.contains("--") || token.isEmpty())
					continue;
				else {
					blocksList.add(token.replace(";", DEFAULT_STATEMENT_SEPARATOR));
				}

			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return blocksList;

	}
	
	

	

	public static void main(String[] args) {
		ScriptSeprator scriptSeprator = new ScriptSeprator();
		List<String> blockExtract = scriptSeprator
				.blockExtract(new File("D:/fwin01977/flywaydb/db/migrations/R_cb_synonyms.sql"));
		System.out.println(blockExtract);
	}

}

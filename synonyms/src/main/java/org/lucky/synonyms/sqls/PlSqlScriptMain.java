package org.lucky.synonyms.sqls;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PlSqlScriptMain {

	public static String BLOCK_COMMENT_START_PREFIX = "/*";
	public static String BLOCK_COMMENT_END_PREFIX = "*/";

	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(new File("D:/fwin01977/flywaydb/db/migrations/R_cb_synonyms.sql"));
		String block = null;
		List<String> blocksList = new ArrayList<String>();

		while (scanner.hasNextLine()) {
			String token = scanner.nextLine();

			if (token.contains("DECLARE")) {
				block = token;
				while (!token.contains("/")) {
					token = scanner.nextLine();
					block = block + "\n" + token;
				}
				blocksList.add(block);
			}

			else if (token.contains(BLOCK_COMMENT_START_PREFIX)) {
				while ((token = scanner.nextLine()) != null) {
					if (token.contains(BLOCK_COMMENT_END_PREFIX))
						break;
				}
			} else if (token.contains("--") || token.isEmpty())
				continue;
			else {
				blocksList.add(token);
			}

		}
		scanner.close();
	}

}

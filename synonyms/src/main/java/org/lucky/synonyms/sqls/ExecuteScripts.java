package org.lucky.synonyms.sqls;

import java.io.File;
import java.io.FilenameFilter;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Service;

@Service
public class ExecuteScripts {
	
	@Value("${script.location}")
	private String scriptLocation;
	
	@Value("${script.type}")
	private String scriptType;
	
	@Autowired
	private Connection connection;
	
	@Autowired private ScriptSeprator scriptSeprator;
	
	
	public void run() {
		System.out.println("Script location:: "+scriptLocation);
		
		/**
		 * Reading all files from folder
		 */
		File initialFolder = new File(scriptLocation);
		File[] listFiles = initialFolder.listFiles(fileFilter());
		for(File sqlScriptFile : listFiles) {
			    try {
			    	List<String> blockExtract = scriptSeprator.blockExtract(sqlScriptFile);
			    	for(String script : blockExtract) {
			    		ByteArrayResource byteArrayResource = new ByteArrayResource(script.getBytes());
				    	EncodedResource encodedResource =new EncodedResource(byteArrayResource);	
				    	ScriptUtils.executeSqlScript(connection, encodedResource,
				    			false,
				    			false, 
				    			"--",
				    			"/",
				    			"/*",
				    			"*/");
				    	System.out.println("script executed :: "+script);
			    	}
			    	
			    	
			    }catch (Exception e) {
					System.out.println("Error while executing file ::"+sqlScriptFile);
					System.out.println(e.getMessage());
				}
				
				
		}
	   
	}
	
	public FilenameFilter fileFilter() {
		FilenameFilter sqlFileFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.endsWith(scriptType)) {
					return true;
				} else {
					return false;
				}
			}
		};
		return sqlFileFilter;
	}

}

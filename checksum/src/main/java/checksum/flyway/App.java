package checksum.flyway;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import checksum.org.db.Checksum;
import checksum.org.db.controller.DButilityController;

@Configuration
@ComponentScan(basePackages = "checksum")
@EnableTransactionManagement
@PropertySource("file:/tmp/checksum-prop/application.properties")
@SpringBootApplication
public class App implements CommandLineRunner{
	
	@Autowired 
	private LoadableResource loadableResource;
	@Value("${deleteFiles}")
	private String deleteFiles;
	@Autowired
	private Checksum checksum;
	
	@Value("${script.location}")
	private String scriptLocation;
	
	@Value("${import.from.dbutility}")
	private boolean importFromDBUtility;
	
	
	
	
	
	@Autowired private DButilityController dButilityController;
	@SuppressWarnings({"resource" })
	public static void main(String[] args)  {		
		SpringApplication.run(App.class);
	}
	
	

	@Override
	public void run(String... args) throws Exception {
		
		if(importFromDBUtility)
		  dButilityController.migrateData();
		/*Path directoryName = Paths.get(scriptLocation);
		List<File> files = new ArrayList<File>();
		FileUtilities.listf(directoryName, files);

		for (File file : files) {
			List<Integer> fetchChecksum = checksum.fetchChecksum(file.getName());
			if (!fetchChecksum.isEmpty()) {
				System.out.println("DB checksum " + fetchChecksum.toString());
				Integer fileChecksum = loadableResource.checksum(file.toString());
				System.out.println("calculted checksumed " + fileChecksum);
				if (fetchChecksum.contains(fileChecksum)) {
					System.out.println("Patch already applied for file :: " + file.getName());
					// update record
					checksum.updatedChecksum(file.getName());
					if (deleteFiles.equalsIgnoreCase("true"))
						file.delete();
				}
			}
		}*/
		
	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}

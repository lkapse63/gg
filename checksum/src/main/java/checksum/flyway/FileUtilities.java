package checksum.flyway;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

public class FileUtilities {
	
	public static List<File> listf(Path directoryName, List<File> files) {
	    File directory = new File(directoryName.toString());
	    File[] fList = directory.listFiles();
	    if(fList != null)
	        for (File file : fList) {      
	            if (file.isFile()) {
	                files.add(file);
	            } else if (file.isDirectory()) {
	                listf(file.toPath(), files);
	            }
	        }
		return files;
	    }

}

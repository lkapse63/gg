package checksum.org.db.primary.repo;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import checksum.org.db.primary.entity.SchemaHistory;

@Repository
public class SchemaHistoryRepoImpl implements SchemaHistoryRepo {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Value("${fetch.schmahistory.all}")
	private String fetchAll;
	
	@Value("${schema.table}")
	private String schemaTable;
	
	@Value("${next.value.query}")
	private String nextValQuery;
	
	@Override
	@Transactional
	public SchemaHistory findById(int id) {
		return (SchemaHistory) sessionFactory.getCurrentSession()
				.createSQLQuery(String.format(fetchAll, schemaTable))
				.addEntity( SchemaHistory.class)
		        .list().get(0);
	}

	@Override
	@Transactional
	public void save(SchemaHistory schemaHistory) {
		sessionFactory.getCurrentSession().save(schemaHistory);
		
	}
	
	@Override
	@Transactional
	public void saveOrUpdate(SchemaHistory schemaHistory) {
		sessionFactory.getCurrentSession().saveOrUpdate(schemaHistory);
		
	}

	@Override
	@Transactional
	public int nextVal() {
		Integer id=1;
		try {
			String query = String.format(nextValQuery, schemaTable);
			 List<BigDecimal> nextVal= (List<BigDecimal>)sessionFactory.getCurrentSession()
			.createSQLQuery(query)
			.list();	
			 id += Integer.valueOf(nextVal.get(0).toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
	}

}

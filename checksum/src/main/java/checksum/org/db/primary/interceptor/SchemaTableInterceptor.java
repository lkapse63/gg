package checksum.org.db.primary.interceptor;

import javax.annotation.PostConstruct;

import org.hibernate.EmptyInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope("prototype")
public class SchemaTableInterceptor extends EmptyInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Value("${schema.table}")
	private String schemaTable;
	
	@Override
	public String onPrepareStatement(String sql) {
		String prepedStatement = super.onPrepareStatement(sql);
		
		prepedStatement = prepedStatement.replaceAll("SchemaHistory", schemaTable);
		return prepedStatement;
	}
	
	
	@PostConstruct
	public void updateTableName() {
		schemaTable ="\""+schemaTable+"\"";
	}

}

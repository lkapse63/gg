package checksum.org.db.primary.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(schema="FLYWAY_SCHEMA_HISTORY")
public class SchemaHistory {

	@Id
	@Column(name = "\"installed_rank\"",updatable = false, nullable = false)
	private int id;

	@Column(name = "\"version\"")
	private String version;
	@Column(name = "\"description\"")
	private String description;
	@Column(name = "\"type\"")
	private String type;
	@Column(name = "\"script\"")
	private String script;
	@Column(name = "\"checksum\"")
	private int checksum;
	@Column(name = "\"installed_by\"")
	private String installed_by;
	@Column(name = "\"installed_on\"")
	private Date installed_on;
	@Column(name = "\"execution_time\"")
	private int execution_time;
	@Column(name = "\"success\"")
	private int success;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public int getChecksum() {
		return checksum;
	}

	public void setChecksum(int checksum) {
		this.checksum = checksum;
	}

	public String getInstalled_by() {
		return installed_by;
	}

	public void setInstalled_by(String installed_by) {
		this.installed_by = installed_by;
	}

	public Date getInstalled_on() {
		return installed_on;
	}

	public void setInstalled_on(Date installed_on) {
		this.installed_on = installed_on;
	}

	public int getExecution_time() {
		return execution_time;
	}

	public void setExecution_time(int execution_time) {
		this.execution_time = execution_time;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "SchemaHistory [id=" + id + ", version=" + version + ", description=" + description + ", type=" + type
				+ ", script=" + script + ", checksum=" + checksum + ", installed_by=" + installed_by + ", installed_on="
				+ installed_on + ", execution_time=" + execution_time + ", success=" + success + "]";
	}

}

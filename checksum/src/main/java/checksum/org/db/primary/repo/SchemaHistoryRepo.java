package checksum.org.db.primary.repo;

import checksum.org.db.primary.entity.SchemaHistory;


public interface SchemaHistoryRepo {
	
	SchemaHistory findById(int id);
	
	void save(SchemaHistory schemaHistory);
	
	void saveOrUpdate(SchemaHistory schemaHistory);
	
	int nextVal();

}

package checksum.org.db.secondary.repo;

import java.util.List;

public interface DBDeploy<DBUtility>{
	
	List<DBUtility> fetchAllRecord();

}

package checksum.org.db.secondary.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import checksum.org.db.secondary.entity.DBUtility;

@Repository
public class DBDeployImpl implements DBDeploy<DBUtility> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public List<DBUtility> fetchAllRecord() {
		String hql="from DBUtility";
		return sessionFactory.getCurrentSession().createQuery(hql)
				.list();
	}
}

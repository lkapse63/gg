package checksum.org.db.secondary.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="T_DBDEPLOY_CHANGE_LOG")
public class DBUtility {
	
	@Id
	@GeneratedValue
	@Column(name="CHANGE_NUMBER")
	private int id;
	
	@Column(name="COMPLETE_DT")
	private Date completeDT;
	
	@Column(name="APPLIED_BY")
	private String appliedBy;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="CHANGE_STAGE")
	private String changeState;
	
	@Transient
	private int checksum;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCompleteDT() {
		return completeDT;
	}

	public void setCompleteDT(Date completeDT) {
		this.completeDT = completeDT;
	}

	public String getAppliedBy() {
		return appliedBy;
	}

	public void setAppliedBy(String appliedBy) {
		this.appliedBy = appliedBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getChangeState() {
		return changeState;
	}

	public void setChangeState(String changeState) {
		this.changeState = changeState;
	}
	

	public int getChecksum() {
		return checksum;
	}

	public void setChecksum(int checksum) {
		this.checksum = checksum;
	}

	@Override
	public String toString() {
		return "DBUtility [id=" + id + ", completeDT=" + completeDT + ", appliedBy=" + appliedBy + ", description="
				+ description + ", changeState=" + changeState + ", checksum=" + checksum + "]";
	}

	
	

}

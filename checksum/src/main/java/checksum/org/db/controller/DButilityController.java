package checksum.org.db.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import checksum.org.db.primary.entity.SchemaHistory;
import checksum.org.db.secondary.entity.DBUtility;
import checksum.org.db.service.TransferData;

@Controller
public class DButilityController {

	@Autowired private TransferData transferData;
	
	public void migrateData() {
		List<DBUtility> dbUtility = transferData.modifyDBUtility();
		dbUtility = transferData.addChecksum(dbUtility);
		List<SchemaHistory> flywayHistoryData = transferData.crateFlywayHistoryData(dbUtility);
		transferData.storeSchemaHistoryData(flywayHistoryData);
	}
}

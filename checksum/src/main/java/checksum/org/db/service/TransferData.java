package checksum.org.db.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import checksum.flyway.LoadableResource;
import checksum.org.db.primary.entity.SchemaHistory;
import checksum.org.db.primary.repo.SchemaHistoryRepo;
import checksum.org.db.secondary.entity.DBUtility;
import checksum.org.db.secondary.repo.DBDeploy;

@Service
public class TransferData {
	
	@Autowired
	private DBDeploy<DBUtility> DdBDeploy;
	
	@Autowired private SchemaHistoryRepo schemaHistoryRepo;
	
	@Autowired 
	private LoadableResource loadableResource;
	
	@Value("${prefix.version}")
	private String prefixVersion;
	
	@Value("${script.location}")
	private String scriptLocation;
	
	public List<DBUtility> modifyDBUtility() {
		List<DBUtility> dBUtilityList = DdBDeploy.fetchAllRecord();
		
		for(DBUtility dbUtility : dBUtilityList)
			dbUtility.setDescription(prefixVersion+dbUtility.getDescription());
		
		return dBUtilityList;
				
	}
	
	public List<DBUtility> addChecksum(List<DBUtility> dBUtilityList){
		for(DBUtility dbUtility : dBUtilityList) {
			Integer fileChecksum = loadableResource.checksum(scriptLocation+File.separator+dbUtility.getDescription());
			dbUtility.setChecksum(fileChecksum);
		}
			
		return dBUtilityList;
	}

	public List<SchemaHistory> crateFlywayHistoryData(List<DBUtility> dBUtilityList) {
		List<SchemaHistory> schemaList =new ArrayList<SchemaHistory>();
		int nextIndex = schemaHistoryRepo.nextVal();
		for(int i=0; i < dBUtilityList.size(); i++) {
			DBUtility dbUtility = dBUtilityList.get(i);
			String version=dbUtility.getDescription().split("[_\\s__]")[0].replace(prefixVersion, "");
			String discription=dbUtility.getDescription().replace(prefixVersion+version, "").replaceAll("[_\\s__]", " ").trim();
			SchemaHistory history =new SchemaHistory();
			history.setChecksum(dbUtility.getChecksum());
			history.setDescription(discription);
			history.setExecution_time(1);
			history.setInstalled_by(dbUtility.getAppliedBy());
			history.setInstalled_on(dbUtility.getCompleteDT());
			history.setScript(dbUtility.getDescription());
			history.setSuccess(1);
			history.setType("SQL");
			history.setVersion(version);
			history.setId(nextIndex+(i));
			schemaList.add(history);
		}
		return schemaList;
	}
	
	
	public void storeSchemaHistoryData(List<SchemaHistory> schemaHistoryList) {
		for(SchemaHistory schemaHistory : schemaHistoryList)
		    schemaHistoryRepo.save(schemaHistory);
	}
}

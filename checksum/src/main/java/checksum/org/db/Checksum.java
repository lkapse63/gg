package checksum.org.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import checksum.org.db.primary.entity.SchemaHistory;
import checksum.org.db.primary.repo.SchemaHistoryRepo;

@Repository
public class Checksum {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/*@Autowired
	@Qualifier("primaryEntityManagerFactory")
	private EntityManagerFactory entityManagerFactory;*/
	
	@Value("${schema.table}")
	private String tableName;
	
	@Value("${fetch.checksum}")
	private String queryString;
	
	@Value("${fetch.schmahistory.single}")
	private String fetchSingleRecord;
	
	@Autowired private SchemaHistoryRepo schemaHistoryRepo;
	
	
	 @SuppressWarnings("unchecked")
	 @Transactional
	public List<Integer> fetchChecksum(String fileName) {
		 List<Integer> checksums= new ArrayList<Integer>();
		try {
			
			String query= String.format(queryString, tableName,fileName);
			
			List<java.math.BigDecimal> resultList = (List<BigDecimal>)
					sessionFactory
					.getCurrentSession()
			.createSQLQuery(query)
			.list();			
			 for(BigDecimal obj : resultList) {
			     Integer id = Integer.valueOf(obj.toString());
			     checksums.add(id);
			}
		}catch (Exception e) {
			System.err.println(e.getMessage());
		}
		 
		return checksums;
		
	}
	 @SuppressWarnings("unchecked")
	 @Transactional
	 public void updatedChecksum(String fileName) {
		 String query= String.format(fetchSingleRecord, tableName,fileName);
		
		List<SchemaHistory> resultList = sessionFactory
			.getCurrentSession()
	        .createSQLQuery(query)
	        .addEntity(SchemaHistory.class)
	        .list();
		
		for(SchemaHistory schema : resultList) {
			schema.setSuccess(1);
			schema.setInstalled_on(new Date());
			schemaHistoryRepo.saveOrUpdate(schema);
		}
					
	 }

}

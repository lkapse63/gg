/*package checksum.org.db.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(entityManagerFactoryRef = "secondaryEntityManagerFactory",
//	basePackages = { "checksum.org.db.secondary" }
//)
public class SecondaryDBConfiguration {

	
	@Bean(name = "secondaryDataSource")
	@ConfigurationProperties(prefix = "secondary")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	
	@Bean(name = "secondaryEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("secondaryDataSource") DataSource dataSource) {
		
		return builder.dataSource(dataSource)
				.packages("checksum.org.db.secondary")
				.persistenceUnit("secondary")
				.build();
	}

	
	@Bean(name = "secondaryTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("secondaryEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
	
	@Bean(name="secondarySession")
	public Session getSessionFactory(@Qualifier("secondaryEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return entityManagerFactory.createEntityManager().unwrap(org.hibernate.Session.class);
		
	}
}
*/
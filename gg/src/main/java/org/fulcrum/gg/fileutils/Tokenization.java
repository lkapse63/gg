package org.fulcrum.gg.fileutils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fulcrum.gg.utilty.AppUtility;

public class Tokenization {
	
	public static List<String> replaceTokensFromLine(Set<String> tokenSet, List<String> filelines,String module){
		Map<String, String> tokenMap = new HashMap<String, String>();
		ArrayList<String> fileContent = new ArrayList<String>();
		for (String tokenKey : tokenSet) {
			String property = ReadProperties.getProperty(tokenKey);
			if (property != null)
				tokenMap.put(tokenKey, property);
		}
		if (tokenMap.isEmpty())
			return filelines;
		System.out.println("token map :: " + tokenMap.toString());
		
		for (String line : filelines) {
			if (line.contains("X_")) {
				fileContent.add(AppUtility.replceTokens(line, module, tokenMap));
			} else {
				fileContent.add(line);
			}
		}
		return fileContent;
	}
	
	
	public static List<String> replaceTokensFromLine(Set<String> tokenSet, List<String> filelines){
		Map<String, String> tokenMap = new HashMap<String, String>();
		ArrayList<String> fileContent = new ArrayList<String>();
		for (String tokenKey : tokenSet) {
			String mapPropValue = ReadProperties.getMapPropValue(tokenKey);
			if (mapPropValue != null)
				tokenMap.put(tokenKey, mapPropValue);
		}
		if (tokenMap.isEmpty())
			return filelines;
		System.out.println("token map :: " + tokenMap.toString());
		
		for (String line : filelines) {
			if (line.contains("X_")) {
				fileContent.add(AppUtility.replceTokens(line, tokenMap));
			} else {
				fileContent.add(line);
			}
		}
		return fileContent;
	}

}

package org.fulcrum.gg.fileutils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Merger {


	public List<List<String>>  getMapOfFiles(List<String> files) {
		Set<String> tokens = new HashSet<>();
		for (String name : files) {
			String token = "";
			String fName = new File(name).getName();
			if (fName.contains("_common")) {
				token = fName.substring(0, fName.indexOf("_common"));
				tokens.add(token);
			} else {
				token = fName.substring(0, fName.indexOf("."));
				tokens.add(token);
			}

		}
		List<List<String>> list = new ArrayList<>();
		for(String token : tokens) {
			List<String> tmpList=new ArrayList<>();
			for(String name : files) {
				String fName  = new File(name).getName();
				if(fName.startsWith(token)) {
					tmpList.add(name);
				}
				if(!tmpList.isEmpty())
				   list.add(tmpList);
			}
		}
		return list;
	}
	
	
	public Map<String, List<List<String>>>  getCommonFiles(Map<String, List<String>>  moduleMapper) {
		Set<Entry<String, List<String>>> entrySet = moduleMapper.entrySet();
		Iterator<Entry<String, List<String>>> iterator = entrySet.iterator();
		Map<String, List<List<String>>>  mapper=new HashMap<>();
		while (iterator.hasNext()) {
			Entry<String, List<String>> mapEntry = iterator.next();
			List<List<String>> mapOfFiles = getMapOfFiles(mapEntry.getValue());
			mapper.put(mapEntry.getKey(), mapOfFiles);	
		}
		return mapper;
	}
}

package org.fulcrum.gg.fileutils;

import java.io.BufferedWriter;
import static java.nio.file.StandardCopyOption.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUtilities {
	
	
	public static void createDirectory(String path) {
		if(! Files.exists(Paths.get(path)))
			try {
				Files.createDirectories(Paths.get(path));
				System.out.println("Direcoty created "+path);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public static void createDirectory(List<String> paths) {
		for(String path : paths)
			createDirectory(path);
	}


	public static void createFiles(List<String> contents, String fileLocation,boolean append) throws IOException {
//		System.out.println("writing data into "+fileLocation);
		File file = new File(fileLocation);
		if (!file.exists())
			file.getParentFile().mkdirs();
		FileWriter fr = new FileWriter(file, append);
		BufferedWriter br = new BufferedWriter(fr);
		for(String line : contents)
			br.write(line+"\n");
		br.close();
		fr.close();
		
	}
	
	/**
	 * 
	 * @param fileLoc
	 * @throws IOException 
	 */
	public static void combineFiles(List<String> fileList,String dest) throws IOException {
		List<String> filelines=new ArrayList<String>();
		String parentPath=getParent(fileList.get(0));
		String nameOfFile=getName(fileList.get(0));
		for(String file : fileList)
		  filelines.addAll(ReadFilesUtil.readAllFile(Paths.get(file)));
		createFiles(filelines, dest+parentPath+"/"+nameOfFile, false);
	}
	
	public static String getName(String fileLocation) {
		String nameOfFile=fileLocation.substring(fileLocation.lastIndexOf("/")+1);
		return nameOfFile.replace("_common", "");
	}
	
	public static String getParent(String path) {
		File file = new File(path);
		String parent = file.getParent();
		return parent.substring(parent.lastIndexOf(File.separator));
	}
	
	public static void deleteFile(String fileLoc) {
		File file = new File(fileLoc);
		if (!file.exists())
			file.delete();
	}
	
	
	public static void emptyDirectory(String dirLoc) {
		File file =new File(dirLoc);
		if (!file.exists())
			return;
		for(File f : file.listFiles()) {
			if(f.isDirectory())
				emptyDirectory(f.getAbsolutePath());
			f.delete();
		}
			
	}
	
	public static void copyFile(String from, String to) throws IOException{
        Path src = Paths.get(from);
        new File(to).mkdirs();
        for(File file :src.toFile().listFiles())
            Files.copy(file.toPath(), Paths.get(to+"/"+file.getName()), REPLACE_EXISTING);
    }


}

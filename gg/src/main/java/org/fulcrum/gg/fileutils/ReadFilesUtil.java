package org.fulcrum.gg.fileutils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReadFilesUtil {

	
	public static  List<String> readAllFile(Path filePath) throws IOException{
		return Files.readAllLines(filePath, Charset.defaultCharset());
	}
	
	public static List<File> listf(Path directoryName, List<File> files) {
	    File directory = new File(directoryName.toString());
	    File[] fList = directory.listFiles();
	    if(fList != null)
	        for (File file : fList) {      
	            if (file.isFile()) {
	                files.add(file);
	            } else if (file.isDirectory()) {
	                listf(file.toPath(), files);
	            }
	        }
		return files;
	    }
	
	public static List<File> listf(Path directoryName, List<File> files,String type) {
	    File directory = new File(directoryName.toString());
	    File[] fList = directory.listFiles();
	    if(fList != null)
	        for (File file : fList) {      
	            if (file.isFile()) {
	                files.add(file);
	            } else if (file.isDirectory()) {
	                listf(file.toPath(), files);
	            }
	        }
	    Iterator<File> iterator = files.iterator();
	    while (iterator.hasNext()) {
			if(!iterator.next().getName().endsWith(type))
				iterator.remove();
			
		}
		return files;
	    }
	
	
	public static String findFilePath(String searchkey, Path baseDir) {
		List<File> fileList = new ArrayList<File>();
		fileList=listf(baseDir, fileList);
		for(File filePath : fileList) {
			if(filePath.toString().contains(searchkey))
				return filePath.toString();
		}
		return searchkey+" Not found";
	}
	
	public static String findFilePath(String searchkey, Path baseDir,String type) {
		List<File> fileList = new ArrayList<File>();
		fileList=listf(baseDir, fileList,type);
		for(File filePath : fileList) {
			if(filePath.toString().contains(searchkey))
				return filePath.toString();
		}
		return searchkey+" Not found";
	}
	
	public static String findFileName(String searchkey, Path baseDir) {
		List<File> fileList = new ArrayList<File>();
		fileList=listf(baseDir, fileList);
		for(File filePath : fileList) {
			if(filePath.getName().toLowerCase().contains(searchkey.toLowerCase()))
				return filePath.toString();
		}
		return searchkey+" Not found";
	}
	
}

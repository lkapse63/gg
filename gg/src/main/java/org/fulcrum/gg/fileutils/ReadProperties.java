package org.fulcrum.gg.fileutils;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class ReadProperties {
	
	public static Properties prop;
	
	public static Map<String,String> propMap= new HashMap<>();
	
	public static Properties init(Path filePath) {
		
		prop= new Properties();
		try {
			prop.load(new FileReader(filePath.toFile()));
		} catch (IOException e) {
			System.err.println("file not found \n"+e);
			
		} 
		return prop;
	}
	
	
	public static String getProperty(String key) {
		return prop.getProperty(key);
	}
	
	public static String getProperty(String key,Path filePath) {
		init(filePath);
		return getProperty(key);
	}
	
	
	public static void initPropertyMap(Path goldenPath) {
		try {
			List<String> filelines = ReadFilesUtil.readAllFile(goldenPath);
			for(String line : filelines) {
				if (line.contains("X_")) {
					String key=line.substring(line.indexOf("X_"),line.indexOf("="));
					String value =line.split("=")[1];
					propMap.put(key, value);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getMapPropValue(String key) {
		return propMap.get(key);
		
	}

}

package org.fulcrum.gg.etl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.fulcrum.gg.fileutils.FileUtilities;
import org.fulcrum.gg.fileutils.ReadFilesUtil;
import org.fulcrum.gg.fileutils.ReadProperties;
import org.fulcrum.gg.fileutils.Tokenization;
import org.fulcrum.gg.utilty.AppUtility;
import org.fulcrum.gg.utilty.Constant;

public class ProcessObys {
	
	public static void processObyes(Path goldenGatePropertyPath, Path baseModulePath, String tempFileLocation) throws Exception {
		
		Path appUserConfigPath = Paths
				.get(ReadFilesUtil.findFilePath("app-user-config.properties", goldenGatePropertyPath));
		System.out.println("app-user-config.properties location  :: " + appUserConfigPath);

		ReadProperties.init(appUserConfigPath);
		String goldengateModules = ReadProperties.getProperty("goldengate.modules");
		String targetEnv = ReadProperties.getProperty("targetEnv");

		Path ggMasterConfigPath = Paths.get(ReadFilesUtil.findFileName(targetEnv, goldenGatePropertyPath));
		System.out.println("gg config location  :: " + ggMasterConfigPath);
		ReadProperties.initPropertyMap(ggMasterConfigPath);

		System.out.println("Golden gate modules  :: " + goldengateModules + " \ntargetEnv ::  " + targetEnv);

		ReadProperties.init(ggMasterConfigPath);
		/**
		 * Golden gate App modules array
		 */
		String[] modules = goldengateModules.split(",");

		for (String module : modules) {
			String modulePath = ReadFilesUtil.findFilePath(module, baseModulePath);
			if (!modulePath.contains("Not found")) {
				System.out.println("module found :: " + module);
				/**
				 * Retrieve prm file list
				 */
				Path moduleBaseDir = Paths.get(modulePath.substring(0, modulePath.indexOf(module) + module.length())+"/resources/goldengate/etl");
				System.out.println("base module location  :: " + moduleBaseDir);

				/**
				 * Retrieve all files
				 */
				ArrayList<File> fileList = new ArrayList<File>();
				ReadFilesUtil.listf(moduleBaseDir, fileList,"oby");
				Path tokenSrcFilePath = null;

				for (File filePath : fileList) {

					Set<String> tokenSet = new HashSet<String>();
					if (isObyes(module, filePath)) {
						tokenSrcFilePath = filePath.toPath();
						List<String> filelines = ReadFilesUtil.readAllFile(filePath.toPath());
						for (String line : filelines) {
							if (line.contains("X_"))
							   tokenSet.addAll(AppUtility.getTokens(line));
						}

						/**
						 * Tokens found
						 */
						String copyPath = tempFileLocation + "/data/"  + "obyes/"
								+ tokenSrcFilePath.toFile().getName();
						if (!tokenSet.isEmpty()) {
							List<String> fileContent = Tokenization.replaceTokensFromLine(tokenSet, filelines);
							FileUtilities.createFiles(fileContent, copyPath, false);
							/**
							 * Adding files into map
							 */
							AppUtility.addIntoObyesModuleMap(module, copyPath);
						} else {
							FileUtilities.createFiles(filelines, copyPath, false);
							AppUtility.addIntoObyesModuleMap(module, copyPath);
						}

					}

				}

			}
		}
		
		writeObyesFile(tempFileLocation);
		
	}
	
	public static boolean isObyes(String module, File filePath) {
		return filePath.toString().contains(module) && filePath.getName().endsWith(".oby")
				&& (filePath.toString().contains("obeys")) && filePath.toString().contains("goldengate");
	}
	
	
	public static void writeObyesFile(String tempFileLocation) throws IOException {
		if(Constant.OBEYESFILEMAP.isEmpty())
			return;
		String parentPath = null ;
		Set<Entry<String, List<String>>> entrySet = Constant.OBEYESFILEMAP.entrySet();
		Iterator<Entry<String, List<String>>> iterator = entrySet.iterator();
		while(iterator.hasNext()) {
			Entry<String, List<String>> module = iterator.next();
			String prmFileLocation=module.getValue().get(0);
			parentPath = prmFileLocation.substring(0,prmFileLocation.lastIndexOf("/"));
			break;
		}
		ArrayList<File> flist = new ArrayList<File>();
		ReadFilesUtil.listf(Paths.get(parentPath), flist);
		for (File file2 : flist) {
			List<String> filelines = ReadFilesUtil.readAllFile(file2.toPath());
			FileUtilities.createFiles(filelines, tempFileLocation + "/final/obyes/" + file2.getName(), true);
		}
	}

}

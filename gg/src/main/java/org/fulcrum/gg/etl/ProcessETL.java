package org.fulcrum.gg.etl;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.fulcrum.gg.fileutils.FileUtilities;
import org.fulcrum.gg.fileutils.Merger;
import org.fulcrum.gg.fileutils.ReadFilesUtil;
import org.fulcrum.gg.fileutils.ReadProperties;
import org.fulcrum.gg.fileutils.Tokenization;
import org.fulcrum.gg.utilty.AppUtility;
import org.fulcrum.gg.utilty.Constant;

public class ProcessETL {

	public static void doETLTOkenReolce(Path goldenGatePropertyPath, Path baseModulePath, String tempFileLocation)
			throws Exception {
		Path appUserConfigPath = Paths
				.get(ReadFilesUtil.findFilePath("app-user-config.properties", goldenGatePropertyPath));
		System.out.println("app-user-config.properties location  :: " + appUserConfigPath);

		ReadProperties.init(appUserConfigPath);
		String goldengateModules = ReadProperties.getProperty("goldengate.modules");
		String targetEnv = ReadProperties.getProperty("targetEnv");

		Path ggMasterConfigPath = Paths.get(ReadFilesUtil.findFileName(targetEnv, goldenGatePropertyPath));
		System.out.println("gg config location  :: " + ggMasterConfigPath);

		System.out.println("Golden gate modules  :: " + goldengateModules + " \ntargetEnv ::  " + targetEnv);

//		ReadProperties.init(ggMasterConfigPath);
		ReadProperties.initPropertyMap(ggMasterConfigPath);
		/**
		 * Golden gate App modules array
		 */
		String[] modules = goldengateModules.split(",");

		for (String module : modules) {
			String modulePath = ReadFilesUtil.findFilePath(module, baseModulePath,".prm");
			if (!modulePath.contains("Not found")) {
				System.out.println("module found :: " + module);
				/**
				 * Retrieve prm file list
				 */
				Path moduleBaseDir = Paths.get(modulePath.substring(0, modulePath.indexOf(module) + module.length())+"/resources/goldengate");
				System.out.println("base module location  :: " + moduleBaseDir);

				/**
				 * Retrieve all files
				 */
				ArrayList<File> fileList = new ArrayList<File>();
				ReadFilesUtil.listf(moduleBaseDir, fileList,".prm");
				Path tokenSrcFilePath = null;

				for (File filePath : fileList) {

					Set<String> tokenSet = new HashSet<String>();
					if (isPRMFile(module, filePath)) {
						tokenSrcFilePath = filePath.toPath();
						List<String> filelines = ReadFilesUtil.readAllFile(filePath.toPath());
						for (String line : filelines) {
							if (line.contains("X_"))
							    tokenSet.addAll(AppUtility.getTokens(line));
						}

						/**
						 * Tokens found
						 */
						String copyPath = tempFileLocation + "/data/" + module + "/"
								+ tokenSrcFilePath.toFile().getName();
						if (!tokenSet.isEmpty()) {
							List<String> fileContent = Tokenization.replaceTokensFromLine(tokenSet, filelines);
							FileUtilities.createFiles(fileContent, copyPath, false);
							/**
							 * Adding files into map
							 */
							AppUtility.addIntoModuleMap(module, copyPath);
						} else {
							FileUtilities.createFiles(filelines, copyPath, false);
							AppUtility.addIntoModuleMap(module, copyPath);
						}

					}

				}

			}
		}

		/**
		 * Combine Files
		 */
		Merger merger = new Merger();
		Map<String, List<List<String>>> commonFiles = merger.getCommonFiles(Constant.MODULEFILEMAP);
		Iterator<Entry<String, List<List<String>>>> iterator = commonFiles.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, List<List<String>>> mergeFiles = iterator.next();
			for (List<String> fileList : mergeFiles.getValue()) {
				Collections.sort(fileList);
				Collections.reverse(fileList);
				FileUtilities.combineFiles(fileList, tempFileLocation + "/combine");
			}
				
		}

		/**
		 * Creating final files
		 */
		ArrayList<File> flist = new ArrayList<File>();
		Path moduleBaseDir = Paths.get(tempFileLocation + "/combine");

		/**
		 * Empty Directory
		 */
		FileUtilities.emptyDirectory(tempFileLocation + "/final");

		ReadFilesUtil.listf(moduleBaseDir, flist,".prm");
		for (File file2 : flist) {
			List<String> filelines = ReadFilesUtil.readAllFile(file2.toPath());
			FileUtilities.createFiles(filelines, tempFileLocation + "/final/etl/" + file2.getName(), true);
		}
	}

	public static boolean isPRMFile(String module, File filePath) {
		return filePath.toString().contains(module) && filePath.getName().endsWith(".prm")
				&& (filePath.toString().contains("etl")) && filePath.toString().contains("goldengate");
	}

}

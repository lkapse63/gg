package org.fulcrum.gg;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.fulcrum.gg.etl.ProcessETL;
import org.fulcrum.gg.etl.ProcessObys;
import org.fulcrum.gg.fileutils.FileUtilities;

public class UtilityMain {

	public static void main(String[] args) throws Exception {

		Path goldenGatePropertyPath = Paths.get("C:\\Users\\FWIN02122\\Documents\\GoldenGate\\GoldenGate2"); 
		Path baseModulePath = Paths.get("C:\\Users\\FWIN02122\\Documents\\GoldenGate\\GoldenGate2");
		
		System.out.println("args lengh:: "+args.length);
		String tempFileLocation = args.length > 2  ? args[2]: "/tmp/merge";
//		Path goldenGatePropertyPath = Paths.get(args[0]);
//		Path baseModulePath = Paths.get(args[1]);

		FileUtilities.emptyDirectory(tempFileLocation);
		System.out.println("############### ETL START #######################");
		ProcessETL.doETLTOkenReolce(goldenGatePropertyPath, baseModulePath, tempFileLocation);
		System.out.println("############### OBYES START #######################");
		ProcessObys.processObyes(goldenGatePropertyPath, baseModulePath, tempFileLocation);
	}

}


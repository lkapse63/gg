package org.fulcrum.gg.flyway;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class CleanComments {

	public static void main(String[] args) throws IOException {

		String inDir = "D:/fwin01977/flywaydb/script";
		String outDir = inDir + "/out";
		String specialChar="\u0096";
		System.out.println("specialChar "+specialChar);
		new CleanComments().FileClearner(inDir, outDir,specialChar);

	}

	public void FileClearner(String inDir, String outDir,String specialChar) throws IOException {
		if (!Files.exists(Paths.get(outDir)))
			Files.createDirectories(Paths.get(outDir));
		DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(Paths.get(inDir));
		
		for (Path filePath : newDirectoryStream) {
			if (!filePath.toString().endsWith(".sql"))
				continue;
			List<String> stream = Files.readAllLines(filePath, StandardCharsets.UTF_8);
			List<String> listLines = new ArrayList<>();
			for (String line : stream) {
				if (line.toLowerCase().startsWith("comment") && line.contains("\'")) {
					String clean = line.substring(line.indexOf("\'")).replaceAll("[^a-zA-Z0-9\\s+]", " ");
					listLines.add(line.substring(0, line.indexOf("\'")) + "'" + clean.trim() + "'");
				} else {
					if (!line.toLowerCase().contains("values")) {
						listLines.add(line);
						continue;
					}
					if (line.contains("'") && line.contains(")")) {
						String[] vlaues = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")")).split(",");
						StringBuffer buff = new StringBuffer();
						for (String s : vlaues) {
							if (s.contains(specialChar)) {
								String[] split = s.split(specialChar);
								String s2 = "";
								for (int i = 0; i < split.length; i++) {
									if (i == split.length - 1)
										s2 += split[i].trim();
									else
										s2 += split[i].trim() + specialChar;
								}
								buff.append(s2 + ",");
							} else {
								buff.append(s + ",");
							}
						}
						listLines.add(
								line.substring(0, line.indexOf("(")) + "(" + buff.substring(0, buff.length() - 1)
										+ ")");
					} else {
						listLines.add(line);
					}
				}
			}
			Path out = Paths.get(outDir + "/" + filePath.getFileName());
			Files.write(out, listLines, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
			System.out.println("file done " + out);
			listLines.clear();

		}

	}

}
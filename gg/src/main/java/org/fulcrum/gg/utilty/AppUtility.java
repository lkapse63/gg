package org.fulcrum.gg.utilty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AppUtility {

	
	public static Set<String> getTokens(String line, String module) {
		Set<String> tokenList = new HashSet<>();
		/*
		 * String[] tokens = line.split("\\s"); for(String toke : tokens) {
		 * if(toke.contains("X_")) { int startIndex=toke.indexOf("X_");
		 * toke=toke.substring(startIndex); int endIndex=toke.indexOf("[^a-zA-Z]");
		 * toke=toke.substring(0,endIndex); tokenList.add(module+"."+toke); } }
		 */
		String[] tokens = line.split("[\"\\s@&.?,$+-]+");
		for (String token : tokens) {
			if (token.contains("X_")) {
				String tok = token.substring(token.indexOf("X_"));
				tokenList.add(module + "." + tok);
			}
		}
		return tokenList;
	}
  
  
	public static Set<String> getTokens(String line) {
		Set<String> tokenList = new HashSet<>();
		String[] tokens = line.split("[\"\\s@&.?,$+-]+");
		for (String token : tokens) {
			if (token.contains("X_")) {
				String tok = token.substring(token.indexOf("X_"));
				tokenList.add(tok);
			}
		}
		return tokenList;
	}
  
  
  public static String replceTokens(String line,String module,Map<String, String> tokenMap){
		for (String tokens : getTokens(line)) {
			line = tokenMap.get(module + "." + tokens) !=null ? line.replace(tokens,tokenMap.get(module + "." + tokens)) : line;
		}
		return line;
	}
  
  
  public static String replceTokens(String line,Map<String, String> tokenMap){
		for (String tokens : getTokens(line)) {
			String replaceChar=tokenMap.get(tokens) !=null ? tokenMap.get(tokens) : tokens;
			line = line.replace(tokens,replaceChar );
		}
		return line;
	}
  
  public static void addIntoModuleMap(String module,String file) {
	  List<String> fileList=new ArrayList<>();
	  fileList.add(file);
	  if(Constant.MODULEFILEMAP.get(module) ==null)
		  Constant.MODULEFILEMAP.put(module, fileList);
	  else
		  Constant.MODULEFILEMAP.get(module).addAll(fileList);
  }
  
  public static void addIntoObyesModuleMap(String module,String file) {
	  List<String> fileList=new ArrayList<>();
	  fileList.add(file);
	  if(Constant.OBEYESFILEMAP.get(module) ==null)
		  Constant.OBEYESFILEMAP.put(module, fileList);
	  else
		  Constant.OBEYESFILEMAP.get(module).addAll(fileList);
  }
  
  

	
	public static void main(String[] args) {
		String line="MAP SP22_APMDM.t_md_file_header, TARGET X_ROACDW_APMDMDS_TARGET_SCHEMA.t_md_file_header,";
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ap-mdm-datastore-database.X_ROACDW_MDM_SOURCE_SCHEMA", "SP22_APMDM");
		tokenMap.put("ap-mdm-datastore-database.X_ROACDW_APMDMDS_TARGET_SCHEMA", "SP22_APMDMDS");
		String module="ap-mdm-datastore-database";
		
		System.out.println(replceTokens(line,module,tokenMap));
	}
}

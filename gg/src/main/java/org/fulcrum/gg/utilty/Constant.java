package org.fulcrum.gg.utilty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constant {
	


	
	public static final Map<String, String> moduleMapper = new HashMap<String, String>() {
	
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put("ap-mdm-core-database", "apcore.copy.dir");
			put("ap-core-database", "apcore.copy.dir");
			put("ap-mdm-datastore-database", "apcore.copy.dir");
			put("ap-datastore-database", "apcore.copy.dir");
		}
	};
	
	
	public static final Map<String, String> PRMMAPPER = new HashMap<String, String>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2672945800146175712L;

		{
			put("defgen_common.prm", "defgen.prm");
			put("eoac_common.prm", "eoac.dir");
			put("poacab_common.prm", "poacab.dir");
			put("poacai_common.prm", "poacai.dir");
			put("poacab_common.prm", "poacab.dir");
			put("poacdw_common.prm", "poacdw.dir");
		}
	};
	
	
	
	public static  Map<String, List<String>> MODULEFILEMAP = new HashMap<String, List<String>>();
	public static  Map<String, List<String>> OBEYESFILEMAP = new HashMap<String, List<String>>();

}
